var config = {
    paths: {
        'flexsliderjs': 'Kowal_Slider/js/jquery.flexslider-min'
    },
    shim: {
        'flexsliderjs': {
            deps: ['jquery']
        }
    }
}