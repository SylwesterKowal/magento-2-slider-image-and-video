<?php
declare(strict_types=1);

namespace Kowal\Slider\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class HeroSlider extends Template implements BlockInterface
{

    protected $_template = "widget/heroslider.phtml";
    protected $_path = "/pub/media/homepage/";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * HomePageBaners constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);

        $folder = $this->_scopeConfig->getValue('heroslider/general/folder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (!empty($folder)) $this->_path = $folder;
    }

    public function isSliderEnable()
    {
        return $this->_scopeConfig->getValue('heroslider/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSliderData($group_id)
    {
        $image = $this->_scopeConfig->getValue('heroslider/' . $group_id . '/file', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return [
            'title' => $this->_scopeConfig->getValue('heroslider/' . $group_id . '/title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'button' => $this->_scopeConfig->getValue('heroslider/' . $group_id . '/button', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'url' => $this->_scopeConfig->getValue('heroslider/' . $group_id . '/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'enable' => $this->_scopeConfig->getValue('heroslider/' . $group_id . '/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'image' => (!empty($image)) ? $this->_path . $image : null
        ];
    }

    public function getVideoData()
    {
        $image = $this->_scopeConfig->getValue('heroslider/video/image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $video1 = $this->_scopeConfig->getValue('heroslider/video/file1', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $video2 = $this->_scopeConfig->getValue('heroslider/video/file2', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $video3 = $this->_scopeConfig->getValue('heroslider/video/file3', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return [
            'title' => $this->_scopeConfig->getValue('heroslider/video/title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'button' => $this->_scopeConfig->getValue('heroslider/video/button', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'url' => $this->_scopeConfig->getValue('heroslider/video/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'enable' => $this->_scopeConfig->getValue('heroslider/video/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'image' => (!empty($image)) ? $this->_path . $image : null,
            'file1' => (!empty($video1)) ? $this->_path . $video1 : null,
            'file2' => (!empty($video2)) ? $this->_path . $video2 : null,
            'file3' => (!empty($video3)) ? $this->_path . $video3 : null
        ];
    }
}

