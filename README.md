# Mage2 Module Kowal Slider

    ``kowal/module-slider``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Slider IMg and Video

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_Slider`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-slider`
 - enable the module by running `php bin/magento module:enable Kowal_Slider`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Aktywny (heroslider/general/enable)

 - file (heroslider/slide1/file)

 - title (heroslider/slide1/title)

 - url (heroslider/slide1/url)

 - button (heroslider/slide1/button)

 - file (heroslider/slide2/file)

 - title (heroslider/slide2/title)

 - url (heroslider/slide2/url)

 - button (heroslider/slide2/button)

 - file (heroslider/slide3/file)

 - title (heroslider/slide3/title)

 - url (heroslider/slide3/url)

 - button (heroslider/slide3/button)

 - title (heroslider/video/title)

 - url (heroslider/video/url)

 - button (heroslider/video/button)

 - file1 (heroslider/video/file1)

 - file2 (heroslider/video/file2)

 - file3 (heroslider/video/file3)


## Specifications

 - Widget
	- HeroSlider


## Attributes



